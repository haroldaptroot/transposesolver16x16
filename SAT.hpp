#pragma once

#include <stdint.h>
#include <bit>
#include <vector>
#include <assert.h>

namespace SAT
{
	using std::vector;
	using std::uint32_t;

	struct Lit 
	{
		uint32_t raw;

		Lit(uint32_t raw) :
			raw(raw) 
		{
		}

		uint32_t var()
		{
			return raw >> 1;
		}

		// true if the literal is negative
		bool sign()
		{
			return raw & 1;
		}

		static Lit from_var(uint32_t var)
		{
			return Lit(var << 1);
		}

		Lit operator~() 
		{
			return Lit(raw ^ 1);
		}

		auto operator<=>(const Lit&) const = default;

		static Lit Invalid()
		{
			return Lit(0xFFFFFFFF);
		}
	};

	enum class l_Bool
	{
		Undef, True, False = -1
	};

	struct CRef
	{
		uint32_t raw;

		CRef(uint32_t raw) : 
			raw(raw)
		{
		}

		static CRef Undefined()
		{
			return CRef(0xFFFFFFFF);
		}
	};

	struct Reason
	{
		CRef source;
		int level;

		Reason(CRef source, int level) :
			source(source),
			level(level)
		{

		}
	};

	struct Clause
	{
		struct {
			uint32_t size : 27;
			uint32_t mark : 2;
			uint32_t learnt : 1;
			uint32_t has_extra : 1;
			uint32_t reloced : 1;
		}                            header;
		union { Lit lit; float activity; uint32_t abstract; CRef rel; } data[0];

		// note: the two watches are in clause[0] and clause[1]
		Lit& operator[](size_t index)
		{
			return *(Lit*)&data[index];
		}

		size_t size()
		{
			return header.size;
		}
	};

	struct Watch
	{
		Lit other;
		CRef clause;

		Watch(CRef clause, Lit other) : 
			other(other), 
			clause(clause)
		{
		}
	};

	struct Solver
	{
		vector<uint32_t> clauses;
		vector<l_Bool> assigns;
		// note: the watchlist of a literal L, refers to clauses that contain L
		// note: the two watcheed literals are in clause[0] and clause[1]
		vector<vector<Watch>> watches;
		vector<Reason> varreason;
		
		vector<Lit> trail;
		vector<int> trail_lim;
		size_t queue_start;

		template<class T>
		void unordered_remove(vector<T>& vec, size_t index)
		{
			vec[index] = vec.back();
			vec.pop_back();
		}

		Clause& clause(CRef index)
		{
			return *(Clause*)&clauses[index.raw];
		}

		l_Bool value(Lit lit)
		{
			uint32_t m = 0 - (lit.raw & 1);
			return (l_Bool)(((uint32_t)(int8_t)assigns[lit.var()] + m) ^ m);
		}

		int decisionLevel()
		{
			return trail_lim.size();
		}





		CRef propagate()
		{
			size_t numberOfPropagations = 0;

			while (queue_start < trail.size()) {
				numberOfPropagations++;
				// get a literal to propagate from the implication queue, and move it into the trail
				Lit p = trail[queue_start++];
				// �p at this point seems like it would be a conflict, but it's actually illegal
				// a conflict would have been earlier, without p making it into the implication queue
				assert(value(p) != l_False);
				// process each clause that watches this literal p (clause contains �p)
				vector<Watch>& watchlist = watches[(~p).raw];
				for (size_t i = 0; i < watchlist.size(); i++)
				{
					Watch W = watchlist[i];
					Lit other = watchlist[i].other;
					// if the other watch is ~p, something is badly wrong
					assert(other != ~p);
					// if the other watch is true, clause is satisfied
					if (value(other) == l_Bool::True)
						continue;

					CRef cr = CRef::Undefined();
					Clause& cl = clause(cr = watchlist[i].clause);
					if (cl[0] == ~p)
						std::swap(cl[0], cl[1]);
					assert(cl[1] == ~p);
					assert(cl[0] == other);

					Lit newWatch = Lit::Invalid();
					size_t indexOfNewWatch = -1;
					for (size_t j = 2; j < cl.size(); j++)
					{
						Lit t = cl[j];
						if (value(t) != l_Bool::False) {
							newWatch = t;
							indexOfNewWatch = j;
							break;
						}
					}

					assert(newWatch != ~p);
					assert(newWatch != other);

					if (newWatch != Lit::Invalid()) {
						// found a new literal to watch
						// ensure that watched literals are in cl[0] and cl[1]
						std::swap(cl[1], cl[indexOfNewWatch]);
						// remove this clause from this watchlist
						unordered_remove(watchlist, i);
						// don't skip a clause on next iteration!
						i = i - 1;
						// add this clause to the watchlist of the new watched literal
						// can reuse the whole Watch because the 'other' watch is still the same
						watches[newWatch.raw].push_back(W);
						// Clause could be unit at this point, since value(other) could be false (under the enqueued assignment)
						// that case will be detected later..
					}
					else {
						// Clause became unit, or conflict has been reached.
						// The 'other' literal is implied to be true under the partial assignment.
						// If it was also implied to be false 
						// (a watched literal can be false if the corresponding implication is still in the queue)
						// then a conflict has been reached
						if (value(other) == l_Bool::False) {
							// Conflict found, current clause triggered it
							// The other enqueued implications do not need further processing,
							// they can be included in the current trail as if they have been processed.
							// Any other conflicts that they could give rise to, are not necessary.
							// The corresponding 'assigns' have already been filled in.
							queue_start = trail.size();
							return cr;
						}
						else {
							enqueueImplication(other, cr);
						}
					}
				}
			}

			return CRef::Undefined();
		}

		void enqueueImplication(Lit p, CRef source)
		{
			assert(value(p) == l_Bool::Undef);
			assigns[p.var()] = p.sign() ? l_Bool::False : l_Bool::True;
			varreason[p.var()] = Reason(source, decisionLevel());
			trail.push_back(p);
		}
	};
}